import {DefaultCrudRepository} from '@loopback/repository';
import {Publicacion, PublicacionRelations} from '../models';
import {MongoDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class PublicacionRepository extends DefaultCrudRepository<
  Publicacion,
  typeof Publicacion.prototype._id,
  PublicacionRelations
> {
  constructor(
    @inject('datasources.mongoDB') dataSource: MongoDbDataSource,
  ) {
    super(Publicacion, dataSource);
  }
}
