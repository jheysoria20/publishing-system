import {Entity, model, property} from '@loopback/repository';

@model()
export class Cuenta extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  username: string;

  @property({
    type: 'string',
    required: true,
  })
  contrasenha: string;

  @property({
    type: 'string',
    required: true,
    id: true
  })
  _id?: string;


  constructor(data?: Partial<Cuenta>) {
    super(data);
  }
}

export interface CuentaRelations {
  // describe navigational properties here
}

export type CuentaWithRelations = Cuenta & CuentaRelations;
